# The One Repo
> Three repositories for the systems programmers in the basement,  
> Seven for the web developers in their clouds,  
> Nine for the data scientists to analyze,  
> One for the Admin on his root server,  
> In the land of Git where the commits lie.  
> One system to rule them all,  
> One system to find them,  
> One system to bring them all and in the net-scape to bind them,  
> In the land of Git where the commits lie.

## What is this?
This is a repository of scripts and docs to define unified 
development guidelines for the various projects of mine or
those where I am involved.