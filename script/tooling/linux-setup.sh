#!/usr/bin/env bash

#region Variables
escape_char=$(printf "\u1b")
gray="\x1b[38;2;128;128;128m"
reset="\x1b[0m"

heading="✵"
option="○"
selection="⊚"
#endregion

#region Package Manager
PKGMAN=""
PKGMANS=(apt yum dnf paru aura pacman)
for PPKGMAN in "${PKGMANS[@]}"; do
    SUCCESS=$(command -v "$PPKGMAN" > /dev/null 2>&1)
    if [[ "$?" -eq 0 ]]; then
        PKGMAN=$PPKGMAN
        break
    fi
done
#endregion

#region Custom Macros
exists() {
  command -v "$1" > /dev/null 2>&1
}

eval_software_spec() {
    local spec="$1"
    local optional=false
    if [[ "$spec" == \** ]]; then
        optional=true
        spec=${spec:1:${#spec}}
    fi
    local program=$(echo "$spec" | cut -d'[' -f1)

    if ! exists "$program"; then
        local raw_sources=$(echo "$spec" | cut -d'[' -f2 | cut -d']' -f1)
        local sources=(${raw_sources//,/ })
        local source_count=${#sources[@]}
        local description=$(echo "$spec" | cut -d']' -f2-)
        description=${description:1:${#description}}
        if $optional; then
            printf "║ ❌ %-16s (%s)\n" "$program" "$description"
        else
            printf "║ ⚠️ %-16s (%s)\n" "$program" "$description"
        fi
        for idx in "${!sources[@]}"; do
            local type=$(echo "${sources[idx]}" | cut -d':' -f1)
            local pkg=""
            if [[ "${sources[idx]}" == *":"* ]]; then
                pkg=$(echo "${sources[idx]}" | cut -d':' -f2-)
            fi

            local tree="├──"
            if [[ "$idx" -eq "$((source_count - 1))" ]]; then
                tree="└──"
            fi

            if [[ "$type" == "sys" ]]; then
                if [[ ! -z "$pkg" ]]; then
                    program=$pkg
                fi
                printf "║ ${gray}${tree} Try installing '%s' with your package manager (%s)${reset}\n" "$program" "$PKGMAN"
            elif [[ "$type" == "gh" ]]; then
                if [[ ! -z "$pkg" ]]; then
                    printf "║ ${gray}${tree} Try installing '%s' from https://github.com/%s${reset}\n" "$program" "$pkg"
                else
                    printf "║ ${gray}${tree} Try installing '%s' from GitHub (https://github.com)${reset}\n" "$program"
                fi
            elif [[ "$type" == "web" ]]; then
                if [[ ! -z "$pkg" ]]; then
                    printf "║ ${gray}${tree} Try installing '%s' from https://%s${reset}\n" "$program" "$pkg"
                else
                    printf "║ ${gray}${tree} Try installing '%s' from the web${reset}\n" "$program"
                fi
            elif [[ "$type" == "ctan" ]]; then
                if [[ ! -z "$pkg" ]]; then
                    printf "║ ${gray}${tree} Try installing '%s' from https://www.ctan.org/pkg/%s${reset}\n" "$program" "$pkg"
                else
                    printf "║ ${gray}${tree} Try installing '%s' from CTAN (https://www.ctan.org)${reset}\n" "$program"
                fi
            fi
        done
    fi
}
#endregion

clear

# Software Setup
printf "\x1b[1m$heading Software Setup\x1b[0m\n╔═══════════════\n"
cat software.txt | while read line; do
    if [ -z "$line" ] || [[ "$line" == \#* ]]; then
        continue
    fi
    eval_software_spec "$line"
done
printf "╚═══════════════\n"
